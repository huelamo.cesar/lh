import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { HotelsService } from '../src/hotels/hotels.service';
import { Hotels } from '../src/database/entities/hotels.entity';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  // let hotelsService: ;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Alive');
  });

  it('/hotels (Post)', async () => {
    return request(app.getHttpServer())
      .post('/hotels')
      .send([{
        latitude: 0,
        longitude: 0,
        name: 'testHotel'
      }])
      .then(result => {
        expect(result.statusCode).toBe(201);
        expect(result.body).toBeInstanceOf(Array);
        const [ { id, latitude, longitude, createdAt, updatedAt, name } ] = result.body;
        expect(typeof id).toBe('number');
        expect(latitude).toBe(0);
        expect(longitude).toBe(0);
        expect(typeof createdAt).toBe('string');
        expect(typeof updatedAt).toBe('string');
        expect(name).toBe('testHotel');
      });
  });
  
  it('/hotels (Post) 400', async () => {
    return request(app.getHttpServer())
      .post('/hotels')
      .send({
        latitude: 0,
        longitude: 0,
        name: 'testHotel'
      })
      .then(result => {
        expect(result.statusCode).toBe(400);
      });
  });

  it('/hotels (Post) 400', async () => {
    return request(app.getHttpServer())
      .post('/hotels')
      .send([{
        latitude: 0,
        longitude: 0,
        name: ''
      }])
      .then(result => {
        expect(result.statusCode).toBe(400);
      });
  });

  it('/hotels (Post) 400', async () => {
    return request(app.getHttpServer())
      .post('/hotels')
      .send([{
        latitude: 0,
        longitude: '0',
        name: 'testHotel'
      }])
      .then(result => {
        expect(result.statusCode).toBe(400);
      });
  });

  it('/hotels (Post) 400', async () => {
    return request(app.getHttpServer())
      .post('/hotels')
      .send([{
        latitude: '0',
        longitude: 0,
        name: 'testHotel'
      }])
      .then(result => {
        expect(result.statusCode).toBe(400);
      });
  });

  it('/hotels (GET)', async () => {
    return request(app.getHttpServer())
      .get('/hotels')
      .query({
        latitude: 0.1,
        longitude: 0.1
      }).then(result => {
        expect(result.statusCode).toBe(200);
        const [ hotel ] = result.body;
        expect(result.body).toBeInstanceOf(Array);
        expect(hotel).toBeDefined();
      })
  });

  it('/hotels (GET)', () => {
    return request(app.getHttpServer())
      .get('/hotels')
      .query({
        latitude: 0.1,
        longitude: 0.1,
        distance: 5
      })
      .expect(200)
      .expect([]);
  });

  it('/bookings (GET)', () => {
    return request(app.getHttpServer())
      .get('/bookings')
      .query({
        hotel: 1
      })
      .expect(200)
      .expect([]);
  });

  it('/bookings (POST)', () => {
    return request(app.getHttpServer())
      .post('/bookings')
      .send({
        "checkInDate": "2021-07-09T11:04:46.631Z",
        "checkOutDate": "2021-07-10T11:04:46.631Z",
        "amount": 2,
        "guestName": "string",
        "guestEmail": "huelamo.cesar@gmail.com",
        "guestPhoneNumber": "+34 618907421",
        "hotelId": 1
    }).then(result => {
      expect(result.statusCode).toBe(201);
      expect(typeof result.body.id).toBe('number');
    })
  });

  it('/bookings (POST)', () => {
    return request(app.getHttpServer())
      .post('/bookings')
      .send({
        "checkInDate": "2021-07-09T11:04:46.631Z",
        "checkOutDate": "2021-07-10T11:04:46.631Z",
        "amount": 0,
        "guestName": "string",
        "guestEmail": "huelamo.cesar",
        "guestPhoneNumber": "+34 618907421",
        "hotelId": 1
    })
    .expect(400);
  });

  it('/bookings (POST)', () => {
    return request(app.getHttpServer())
      .post('/bookings')
      .send({
        "checkInDate": "2021-07-11T11:04:46.631Z",
        "checkOutDate": "2021-07-10T11:04:46.631Z",
        "amount": 0,
        "guestName": "string",
        "guestEmail": "huelamo.cesar@gmail.com",
        "guestPhoneNumber": "+34 618907421",
        "hotelId": 1
    })
    .expect(400);
  });

  it('/bookings (POST)', () => {
    return request(app.getHttpServer())
      .post('/bookings')
      .send({
        "checkInDate": "2021-07-09T11:04:46.631Z",
        "checkOutDate": "2021-07-10T11:04:46.631Z",
        "amount": 0,
        "guestName": "string",
        "guestEmail": "huelamo.cesar@gmail.com",
        "guestPhoneNumber": "+34 618907421",
        "hotelId": 200
    })
    .expect(404);
  });


  it('/bookings (POST)', async () => {
    const response = await request(app.getHttpServer())
      .post('/hotels')
      .send([{
        latitude: 0,
        longitude: 0,
        name: 'testHotel'
      }]);
    const [{ id: hotelId }] = response.body;
    const promises = [];
    while (promises.length < 10){
      promises.push(
        request(app.getHttpServer())
        .post('/bookings')
        .send({
          "checkInDate": "2021-07-09T11:04:46.631Z",
          "checkOutDate": "2021-07-10T11:04:46.631Z",
          "amount": 0,
          "guestName": "string",
          "guestEmail": "huelamo.cesar@gmail.com",
          "guestPhoneNumber": "+34 618907421",
          "hotelId": hotelId
        })
      );
    }
    await Promise.all(promises);
    return request(app.getHttpServer())
    .post('/bookings')
    .send({
      "checkInDate": "2021-07-09T11:04:46.631Z",
      "checkOutDate": "2021-07-10T11:04:46.631Z",
      "amount": 0,
      "guestName": "string",
      "guestEmail": "huelamo.cesar@gmail.com",
      "guestPhoneNumber": "+34 618907421",
      "hotelId": hotelId
    })
    .expect(422)
  });
});
