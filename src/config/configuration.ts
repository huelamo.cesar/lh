import * as path from 'path';
import { config } from 'dotenv';
config();

const { DB_HOST, DB_NAME, DB_PASSWORD, DB_USER, NODE_ENV } = process.env;

export default () => ({
  port: 3000,
  database: {
    type: 'postgres',
    host: DB_HOST,
    port: 5432,
    database: DB_NAME,
    password: DB_PASSWORD,
    username: DB_USER,
    entities: [path.resolve(__dirname, '..', '**/**.entity!(*.d).{ts,js}')],
    synchronize: NODE_ENV !== 'production',
    keepConnectionAlive: true
  }
});
