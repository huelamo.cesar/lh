import { Hotels } from "src/database/entities/hotels.entity";
import { HotelDto } from "src/hotels/dtos/hotel.dto";
import { HotelQuery } from "src/hotels/dtos/hotel.query";

export interface IHotelService {
    createHotels(body: HotelDto[]): Promise<Hotels[]>
    listHotels(query: HotelQuery): Promise<Hotels[]>
    getHotel(id: number): Promise<Hotels>
}