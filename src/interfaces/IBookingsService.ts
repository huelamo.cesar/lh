import { BookingDto } from "src/bookings/dtos/booking.dto";
import { Bookings } from "src/database/entities/bookings.entity";

export interface IBookingsService {
    bookRoom(body: BookingDto): Promise<Bookings>
    listBookingsForHotel(hotel: number): Promise<Bookings[]>
}