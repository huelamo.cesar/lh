import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Hotels } from '../database/entities/hotels.entity';
import { Repository } from 'typeorm';
import { HotelsService } from './hotels.service';
import { HotelDto } from './dtos/hotel.dto';


describe('HotelsService', () => {
  let service: HotelsService;
  let repo: Repository<Hotels>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HotelsService,
        {
          provide: getRepositoryToken(Hotels),
          useClass: Repository
        }
      ],
    }).compile();

    service = module.get<HotelsService>(HotelsService);
    repo = module.get<Repository<Hotels>>(getRepositoryToken(Hotels));
  });

  it('should be defined', async () => {
    expect(service).toBeDefined();
  });

  it('should return hotels array', async () => {
    const hotel: Hotels = new Hotels()
    hotel.id = 1;
    hotel.createdAt = new Date();
    hotel.updatedAt = new Date();
    hotel.name = 'testhotel';
    hotel.latitude = 0.0;
    hotel.longitude = 0.0;
    hotel.bookings = [];
    jest.spyOn(repo, 'query').mockImplementationOnce(async query => {
      query = query.replace('\n', '').replace('\t', '').trim();
      return query.startsWith('select *') ? [hotel] : null
    });
    const hotels = await service.listHotels({});
    expect(hotels).toBeInstanceOf(Array);
    expect(hotels[0]).toBeInstanceOf(Hotels);
  })

  it('should create hotels', async () => {
    const hotel: HotelDto = new HotelDto()
    hotel.name = 'testhotel';
    hotel.latitude = 0.0;
    hotel.longitude = 0.0;
    jest.spyOn(repo, 'save').mockImplementationOnce(async body => {
      if (body instanceof Array) {
        return body.map(dto => {
          const dbHotel = new Hotels();
          Object.entries(dto).forEach(([key, val]) => dbHotel[key] = val);
          return dbHotel;
        }) as any;
      } else {
        return new Hotels();
      }
    });
    const hotels = await service.createHotels([hotel]);
    expect(hotels).toBeInstanceOf(Array);
    expect(hotels[0]).toBeInstanceOf(Hotels);
  })


  it('should return one hotel', async () => {
    const hotelId = 1;
    jest.spyOn(repo, 'findOne').mockImplementationOnce(async id => {
        return new Hotels();
    });
    const hotel = await service.getHotel(hotelId);
    expect(hotel).toBeInstanceOf(Hotels);
  })
});
