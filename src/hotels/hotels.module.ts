import { Module } from '@nestjs/common';
import { HotelsService } from './hotels.service';
import { HotelsController } from './hotels.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Hotels } from '../database/entities/hotels.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Hotels])
  ],
  providers: [HotelsService],
  controllers: [HotelsController],
  exports: [HotelsService]
})
export class HotelsModule {}
