import { Body, Controller, Get, ParseArrayPipe, Post, Query } from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Hotels } from '../database/entities/hotels.entity';
import { HotelDto } from './dtos/hotel.dto';
import { HotelQuery } from './dtos/hotel.query';
import { HotelsService } from './hotels.service';

@Controller('hotels')
@ApiTags('Hotels')
export class HotelsController {
    constructor(private readonly hotelsService: HotelsService) {}

    @Post()
    @ApiResponse({ status: 201, type: Hotels, isArray: true, description: 'Hotels created successfully' })
    @ApiResponse({ status: 400, description: 'Wrong payload'})
    @ApiBody({ type: HotelDto, isArray: true })
    createHotels(@Body(new ParseArrayPipe({ items: HotelDto })) body: HotelDto[]) {
        return this.hotelsService.createHotels(body);
    }

    @Get()
    @ApiResponse({ status: 200, type: Hotels, isArray: true})
    @ApiResponse({ status: 400, description: 'Wrong query'})
    listHotels(@Query() query: HotelQuery) {
        return this.hotelsService.listHotels(query);
    }
}
