import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";

export class HotelPatchDto {
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    name?: string;

    @ApiPropertyOptional()
    @IsNumber()
    @IsOptional()
    latitude?: number;

    @ApiPropertyOptional()
    @IsNumber()
    @IsOptional()
    longitude?: number;
}