import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsNumberString, IsOptional, IsString } from "class-validator";

export class HotelQuery {
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    name?: string;

    @ApiPropertyOptional()
    @IsNumberString()
    @IsNotEmpty()
    latitude?: number;

    @ApiPropertyOptional()
    @IsNumberString()
    @IsNotEmpty()
    longitude?: number;
    
    @ApiPropertyOptional({ description: 'Max distance in miles from the coordinates specified. Defaults to 10.', default: '10' })
    @IsNumberString()
    @IsOptional()
    distance?: number;
}