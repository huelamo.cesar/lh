import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Hotels } from '../database/entities/hotels.entity';
import { IHotelService } from '../interfaces/IHotelService';
import { Repository } from 'typeorm';
import { HotelDto } from './dtos/hotel.dto';
import { HotelQuery } from './dtos/hotel.query';

@Injectable()
export class HotelsService implements IHotelService {
    
    constructor(
        @InjectRepository(Hotels)
        private readonly hotelsRepository: Repository<Hotels>
    ) {}

    createHotels(body: HotelDto[]): Promise<Hotels[]> {
        return this.hotelsRepository.save(body);
    }

    listHotels(query: HotelQuery): Promise<Hotels[]> {
        // requires "cube" and "earthdistance" extensions in postgres
        return this.hotelsRepository.query(`
            select *
            from (select *, point(${query.longitude}, ${query.latitude}) <@> point(longitude, latitude) as distance from hotels) a
            where distance < '${query.distance || 10}'
            and name ilike '%${query.name || ''}%';
        `);
    }

    getHotel(id: number): Promise<Hotels> {
        return this.hotelsRepository.findOne(id);
    }
}
