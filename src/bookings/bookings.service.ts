import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Bookings } from '../database/entities/bookings.entity';
import { HotelsService } from '../hotels/hotels.service';
import { IBookingsService } from '../interfaces/IBookingsService';
import { Repository } from 'typeorm';
import { BookingDto } from './dtos/booking.dto';

@Injectable()
export class BookingsService implements IBookingsService {
    
    constructor(
        @InjectRepository(Bookings)
        private readonly bookingsRepository: Repository<Bookings>,
        private readonly hotelsService: HotelsService
    ) {}

    async bookRoom(body: BookingDto): Promise<Bookings> {
        body.hotel = await this.hotelsService.getHotel(body.hotelId);
        if (!body.hotel) {
            throw new NotFoundException(`Hotel with id ${body.hotelId} does not exist`);
        }
        return this.bookingsRepository.save(body);
    }

    listBookingsForHotel(hotel: number): Promise<Bookings[]> {
        return this.bookingsRepository.find({ where: { hotel } });
    }
}
