import { Module } from '@nestjs/common';
import { BookingsService } from './bookings.service';
import { BookingsController } from './bookings.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bookings } from '../database/entities/bookings.entity';
import { HotelsModule } from '../hotels/hotels.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Bookings]),
    HotelsModule
  ],
  providers: [BookingsService],
  controllers: [BookingsController]
})
export class BookingsModule {}
