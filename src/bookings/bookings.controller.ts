import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Bookings } from '../database/entities/bookings.entity';
import { BookingsService } from './bookings.service';
import { BookingDto } from './dtos/booking.dto';
import { BookingsQuery } from './dtos/bookings.query';

@Controller('bookings')
@ApiTags('Bookings')
export class BookingsController {
    constructor( private readonly bookingsService: BookingsService) {}

    @Post()
    @ApiResponse({ status: 201, type: Bookings, description: 'Room booked succesfully' })
    @ApiResponse({ status: 400, description: 'Wrong payload' })
    @ApiResponse({ status: 422, description: 'Hotel fully booked for the night' })
    @ApiBody({ type: BookingDto })
    bookRoom(@Body() body: BookingDto) {
        return this.bookingsService.bookRoom(body);
    }

    @Get()
    @ApiResponse({ status: 200, type: Bookings, isArray: true })
    @ApiResponse({ status: 400, description: 'Wrong payload' })
    listHotelBookings(@Query() query: BookingsQuery) {
        return this.bookingsService.listBookingsForHotel(query.hotel);
    }
}
