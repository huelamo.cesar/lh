import { ApiProperty } from "@nestjs/swagger";
import { IsDateString, IsEmail, IsInt, IsNotEmpty, IsString, Validate } from "class-validator";
import { Hotels } from "../../database/entities/hotels.entity";
import { IsAfterValidation } from "../../decorators/IsAfter.decorator";

export class BookingDto{
    @ApiProperty()
    @IsDateString()
    @IsNotEmpty()
    checkInDate: Date;

    @ApiProperty()
    @IsDateString()
    @IsNotEmpty()
    @Validate(IsAfterValidation, ['checkInDate'])
    checkOutDate: Date;

    @ApiProperty()
    @IsInt()
    @IsNotEmpty()
    amount: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    guestName: string;
    
    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    guestEmail: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    guestPhoneNumber: string;

    @ApiProperty()
    @IsInt()
    @IsNotEmpty()
    hotelId: number;

    hotel: Hotels;
}