import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumberString } from "class-validator";

export class BookingsQuery {
    @ApiProperty()
    @IsNumberString()
    @IsNotEmpty()
    hotel: number;
}