import { Test, TestingModule } from '@nestjs/testing';
import { Bookings } from '../database/entities/bookings.entity';
import { Repository } from 'typeorm';
import { BookingsService } from './bookings.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { HotelsService } from '../hotels/hotels.service';
import { Hotels } from '../database/entities/hotels.entity';
import { BookingDto } from './dtos/booking.dto';
import { NotFoundException } from '@nestjs/common';

describe('BookingsService', () => {
  let service: BookingsService;
  let hotelsService: HotelsService;
  let repo: Repository<Bookings>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingsService,
        {
          provide: getRepositoryToken(Bookings),
          useClass: Repository
        },
        HotelsService,
        {
          provide: getRepositoryToken(Hotels),
          useClass: Repository
        }
      ],
    }).compile();

    service = module.get<BookingsService>(BookingsService);
    repo = module.get<Repository<Bookings>>(getRepositoryToken(Bookings));
    hotelsService = module.get<HotelsService>(HotelsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should throw a 404', async () => {
    jest.spyOn(hotelsService, 'getHotel').mockImplementationOnce(async id => {
      if (typeof id == 'number' && id > 0){
        return new Hotels();
      }
      return null;
    });
    const body: BookingDto = new BookingDto();
    body.hotelId = 0;
    let error = null;
    try{
      await service.bookRoom(body);
    } catch(err) {
      error = err;
    }
    expect(error).toBeInstanceOf(NotFoundException);
  })

  it('should return a booking', async () => {
    jest.spyOn(hotelsService, 'getHotel').mockImplementationOnce(async id => {
      if (typeof id == 'number' && id > 0){
        return new Hotels();
      }
      return null;
    });
    jest.spyOn(repo, 'save').mockImplementationOnce(async body => {
      if (body instanceof Array) {
        return body.map(dto => {
          const dbBooking = new Bookings();
          return dbBooking;
        }) as any;
      } else {
        return new Bookings();
      }
    });
    const body: BookingDto = new BookingDto();
    body.hotelId = 1;
    let error = null;
    const booking = await service.bookRoom(body);
    expect(booking).toBeInstanceOf(Bookings);
  })

  it('should return a booking list', async () => {
    jest.spyOn(repo, 'find').mockImplementationOnce(async (query: any) => {
      if (typeof query?.where?.hotel === 'number' && query.where.hotel > 0) {
        return [new Bookings()];
      } else {
        return []
      }
    });
    const bookings = await service.listBookingsForHotel(1);
    expect(bookings).toBeInstanceOf(Array);
    expect(bookings[0]).toBeInstanceOf(Bookings);
  })
});
