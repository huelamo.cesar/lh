import * as path from 'path';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from './config/configuration';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { HotelsModule } from './hotels/hotels.module';
import { BookingsModule } from './bookings/bookings.module';
import { BookingsSubscriber } from './database/subscribers/bookings.subscriber';

const configFiles = path.resolve(__dirname, 'config', '**', '!(*.d).{ts,js}');

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => configService.get('database') as TypeOrmModuleOptions,
    }),
    HotelsModule,
    BookingsModule,
    BookingsSubscriber
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
