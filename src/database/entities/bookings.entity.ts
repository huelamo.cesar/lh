import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, ManyToOne } from "typeorm";
import { BaseEntity } from "./base.entity";
import { Hotels } from "./hotels.entity";

@Entity()
export class Bookings extends BaseEntity{
    @Column({ type: 'date' })
    @ApiProperty()
    checkInDate: Date;

    @Column({ type: 'date' })
    @ApiProperty()
    checkOutDate: Date;

    @Column({ type: 'int' })
    @ApiProperty()
    amount: number;

    @Column({ type: 'text' })
    @ApiProperty()
    guestName: string;
    
    @Column({ type: 'text' })
    @ApiProperty()
    guestEmail: string;

    @Column({ type: 'text' })
    @ApiProperty()
    guestPhoneNumber: string;

    @ManyToOne(type => Hotels, hotel => hotel.bookings)
    hotel: Hotels;
}