import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, OneToMany } from "typeorm";
import { BaseEntity } from "./base.entity";
import { Bookings } from "./bookings.entity";

@Entity()
export class Hotels extends BaseEntity {
    @Column({ type: 'text' })
    @ApiProperty()
    name: string;

    @Column({ type: 'decimal' })
    @ApiProperty()
    latitude: number;

    @Column({ type: 'decimal' })
    @ApiProperty()
    longitude: number;

    @OneToMany(type => Bookings, booking => booking.hotel)
    bookings: Bookings[];
}