import { Injectable, UnprocessableEntityException } from "@nestjs/common";
import { InjectConnection } from "@nestjs/typeorm";
import { Connection, EntitySubscriberInterface, EventSubscriber, InsertEvent, Repository } from "typeorm";
import { Bookings } from "../entities/bookings.entity";

@Injectable()
@EventSubscriber()
export class BookingsSubscriber implements EntitySubscriberInterface<Bookings> {

    private readonly bookingsRepository: Repository<Bookings>

    constructor(
        @InjectConnection() readonly connection: Connection,
      ) {
        connection.subscribers.push(this);
        this.bookingsRepository = connection.getRepository(Bookings);
      }

    listenTo() {
        return Bookings;
    }

    async beforeInsert(event: InsertEvent<Bookings>) {
        const { checkInDate, checkOutDate, hotel } = event.entity;
        const count = await this.bookingsRepository.count({ where: `
          "hotelId" = '${hotel.id}'
          AND (
            ("checkInDate" < '${checkInDate}' ::timestamp AND "checkOutDate" > '${checkOutDate}'::timestamp)
            OR ("checkInDate" < '${checkOutDate}' ::timestamp AND "checkOutDate" > '${checkInDate}'::timestamp)
          )
        `});
        if (count >= 10) throw new UnprocessableEntityException(`Hotel ${hotel.id} is fully booked for the dates between ${checkInDate} and ${checkOutDate}`);
    }

}