import { ValidatorConstraint, ValidatorConstraintInterface, Validate, ValidationArguments } from 'class-validator';

@ValidatorConstraint()
export class IsAfterValidation implements ValidatorConstraintInterface {
  validate = (value: string, args: ValidationArguments) => {
    const dateAfter = new Date(value);
    const dateBefore = new Date(args.object[args.constraints[0]])
    return dateAfter > dateBefore && (
        dateBefore.getDate() !== dateAfter.getDate()
        || dateBefore.getMonth() !== dateAfter.getMonth()
        || dateBefore.getFullYear() !== dateAfter.getFullYear()
        )  
    }

  defaultMessage = () => 'Check out date should be at least a day later than the check out date';
}

